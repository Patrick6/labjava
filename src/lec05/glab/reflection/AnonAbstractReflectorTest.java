package lec05.glab.reflection;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Date;

/**
 * Created by ag on 10/27/2014.
 */
public class AnonAbstractReflectorTest {
    public static void main(String[] args) {

        //uncomment and see what happens; you can't instantiate an interface
        //UNLESS you implement all of its contracted methods inline
        // ActionListener lis2 = new ActionListener();

        // ActionListener lis1 = new ActionListener();  //can't instantiate an abstract class, EXCEPT anonymously like above



    }
}
